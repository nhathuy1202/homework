#include "iostream"
#include "ctime"
#include <time.h>
#include "cstdlib"
using namespace std;

void wait(int seconds)
{
	clock_t ew;
	ew = clock() + seconds * CLOCKS_PER_SEC;
	while (clock() < ew) {}
}
int buoctieprua() {
	srand(unsigned int(time(NULL)));
	int k;
	k = rand() % 10 + 1;
	if (k >= 1 && k <= 5) return 1;
	else if (k > 5 && k <= 8) return 2;
	else return 3;
}
int buoctieptho(){
	srand(unsigned int(time(NULL)));
	int k;
	k = rand() % 10 + 1;
	if (k <= 2) return 1;
	else if (k > 2 && k <= 4) return 2;
	else if (k > 4 && k <= 5) return 3;
	else if (k > 5 && k <= 8) return 4;
	else return 5;
}
void dichuyenrua(int *quangduong) {
	int a = buoctieprua();
	if (a == 1) *quangduong += 3;
	else if (a == 2)  *quangduong += 1;
	else *quangduong -= 6;
	if (*quangduong < 0) *quangduong = 0;
}
void dichuyentho(int *quangduong) {
	int a = buoctieptho();
	if (a == 1) *quangduong += 0;
	else if (a == 2)  *quangduong += 9;
	else if (a == 3) *quangduong -= 12;
	else if (a == 4) *quangduong += 1;
	else *quangduong -= 2;
	if (*quangduong < 0) *quangduong = 0;
}
void insaumoiluot(int quangduong1, int quangduong2,int i) {
	cout << "Luot: " << i << endl;
	cout << "Quang duong rua di chuyen la: " << quangduong1 << endl;
	cout << "Quang duong tho di chuyen la : " << quangduong2 << endl;
}
bool dua() {
	int quangduong1 = 0;
	int quangduong2 = 0;
	for (int i = 1;; ++i) {
		dichuyenrua(&quangduong1);
		dichuyentho(&quangduong2);
		insaumoiluot(quangduong1, quangduong2, i);
		wait(1);
		if (quangduong1 >= 70 || quangduong2 >= 70) break;
	}
	if (quangduong1 >= 70) return 1;
	else return 0;

}
int main() {
	bool a = dua();
	if (a == 1) cout << "Rua thang !!" << endl;
	else cout << "Tho thang !!" << endl;
	system("pause");
}